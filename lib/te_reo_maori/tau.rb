# frozen_string_literal: true

require_relative 'whakahua'

# Number in Maori
module Tau
  def whakahua(tau)
    raise "Class '#{tau.class}' not implmemented" unless tau.is_a? Integer

    tau.whakahua
  end

  # List numbers
  def self.rarangi
    {
      1 => 'tahi', 2 => 'rua', 3 => 'toru', 4 => 'whā', 5 => 'rima', 6 => 'ono', 7 => 'whitu',
      8 => 'waru', 9 => 'iwa', 10 => 'tekau'
    }
  end

  # Handles multiple digits
  def digit(tau, second_digit)
    return tau if second_digit.nil?

    ((tau - 1) * 10) + second_digit
  end

  # 1
  def tahi
    1
  end

  # 2
  def rua(second_digit = nil)
    digit(2, second_digit)
  end

  # 3
  def toru(second_digit = nil)
    digit(3, second_digit)
  end

  # 4
  def wha(second_digit = nil)
    digit(4, second_digit)
  end

  alias whā wha

  # 5
  def rima(second_digit = nil)
    digit(5, second_digit)
  end

  # 6
  def ono(second_digit = nil)
    digit(6, second_digit)
  end

  def whitu(second_digit = nil)
    digit(7, second_digit)
  end

  def waru(second_digit = nil)
    digit(8, second_digit)
  end

  def iwa(second_digit = nil)
    digit(9, second_digit)
  end

  # 10. Used to add groups of 10
  def tekau(extra_digit = nil)
    extra_digit ? 10 + extra_digit : 10
  end

  # Add extra digit
  def ma(extra_digit)
    extra_digit
  end

  alias mā ma
end
