# frozen_string_literal: true

# Extension to Integer for Maori
class Integer
  def inspect
    whakahua
  end

  # Numeric form of integer
  def tau
    to_s
  end  

  # @param [Integer] upper_part Integer of number of 100s
  # @param [String] lower_part Already translated part less than 100
  def add_parts(upper_part, lower_part)
    if upper_part == 0
      lower_part
    elsif upper_part == 1
      "kotahi rau #{lower_part}"
    else
      "#{upper_part.whakahua} rau #{lower_part}"
    end
  end

  # Pronounce number
  def whakahua
    raise NotImplementedError, "Amount of #{self} implemented yet" if self > 999 || self < 1
    return 'kotahi rau' if self == 100

    lower_part = self % 100
    upper_part = self / 100

    last_digit = lower_part % 10
    last_digit_word = Tau.rarangi[last_digit]

    first_digit = (lower_part / 10)
    if first_digit.positive?
      if last_digit.zero?
        return add_parts(upper_part, 'tekau') if first_digit == 1

        return add_parts upper_part, "#{Tau.rarangi[first_digit]} tekau"
      end

      return add_parts upper_part, "tekau mā #{last_digit_word}" if first_digit == 1

      return add_parts upper_part, "#{Tau.rarangi[first_digit]} tekau mā #{last_digit_word}"
    end
    add_parts upper_part, last_digit_word
  end
end
