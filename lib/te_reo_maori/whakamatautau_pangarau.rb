# frozen_string_literal: true

# Math test
module WhakamatautauPangarau
  # Mathematics. Start a maths test
  def pangarau
    puts 'Nau mai. Please type the answer in Teo Reo Maori (e.g "tahi" for 1) when prompted'
    tau1 = rand(100).to_i
    tau2 = rand(100).to_i
    whakautu_tika = tau1 + tau2
    puts "#{tau1.inspect} + #{tau2.inspect}?"
    whakautu = eval(gets).to_i
    if whakautu == whakautu_tika
      puts 'Ka pai. Correct!'
      ":)"
    else
      puts "Incorrect. You put '#{whakautu.inspect} (#{whakautu})'.
  Correct answer is '#{whakautu_tika.inspect} (#{whakautu_tika})'"
      ":("
    end
  end
end
