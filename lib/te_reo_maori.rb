# frozen_string_literal: true

require 'te_reo_maori/version'
require 'te_reo_maori/tau'
require 'te_reo_maori/whakamatautau_pangarau'
require_relative 'te_reo_maori/whakahua_rorohiko'

module TeReoMaori
  class Error < StandardError; end  
end

extend WhakahuaRorohiko