# frozen_string_literal: true

require_relative 'lib/te_reo_maori/version'

Gem::Specification.new do |spec|
  spec.name          = 'te_reo_maori'
  spec.version       = TeReoMaori::VERSION
  spec.authors       = ['Samuel Garratt']
  spec.email         = ['samuel.garratt@integrationqa.com']

  spec.summary       = 'Tool to aid one to learn Maori and enable one to program in Maori.'
  spec.description   = 'Tool to aid one to learn Maori and enable one to program in Maori.'
  spec.homepage      = 'https://gitlab.com/samuel-garratt/te_reo_maori'
  spec.license       = 'MIT'
  spec.required_ruby_version = Gem::Requirement.new('>= 2.3.0')

  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['source_code_uri'] = 'https://gitlab.com/samuel-garratt/te_reo_maori'
  spec.metadata['changelog_uri'] = 'https://gitlab.com/samuel-garratt/te_reo_maori/ChangeLog'

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    Dir.glob("{lib,exe}/**/*")
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']
end
