# frozen_string_literal: true

RSpec.describe 'māhi whika' do
  before { extend Tau }
  it 'taapirihia te 1 + 1' do
    expect(tahi + rua).to eq(toru)
  end

  it 'kekau 10' do
    expect(tekau).to eq 10
  end

  it 'kekau 11' do
    expect(tekau(ma(tahi))).to eq 11
  end

  it 'taapirihia te 13 + 15' do
    expect((tekau ma toru) + (tekau ma rima)).to eq 28
  end

  it '12 wa 12' do
    expect((tekau ma rua) * (tekau ma rua)).to eq 144
  end

  it 'kekau 25' do
    expect(rua(tekau(ma(rima)))).to eq rua tekau ma rima
  end

  it '43 whakaheke 22' do
    expect((wha tekau ma toru) - (rua tekau ma rua)).to eq 21
  end
end

RSpec.describe 'Whakahua' do
  before { extend Tau }
  it 'toru' do
    expect(3.whakahua).to eq 'toru'
  end

  it 'tekau' do
    expect(10.whakahua).to eq 'tekau'
  end

  it 'tekau mā rua' do
    expect(12.whakahua).to eq 'tekau mā rua'
  end

  it 'takau mā whā' do
    expect(14.whakahua).to eq 'tekau mā whā'
  end

  it 'toru tekau mā rimā' do
    expect(35.whakahua).to eq 'toru tekau mā rima'
  end

  it 'whitu tekau mā waru' do
    expect(78.whakahua).to eq 'whitu tekau mā waru'
  end

  it 'iwa tekau mā iwa' do
    expect(99.whakahua).to eq 'iwa tekau mā iwa'
  end

  it 'kotahi rau' do
    expect(100.whakahua).to eq 'kotahi rau'
  end

  it 'kotahi rau tekau mā tahi' do
    expect(111.whakahua).to eq 'kotahi rau tekau mā tahi'
  end

  it 'rua rau toru tekau mā whā' do
    expect(234.whakahua).to eq 'rua rau toru tekau mā whā'
  end

  it '1000 not done' do
    expect do
      1000.whakahua
    end.to raise_error NotImplementedError
  end

  it 'whakahua tau' do
    expect(whakahua((toru tekau ma tahi) + wha)).to eq 'toru tekau mā rima'
  end
end

RSpec.describe WhakamatautauPangarau do
  before { extend WhakamatautauPangarau }
  it 'pangarau' do
    respond_to? :pangarau
  end
end
