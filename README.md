# TeReoMaori

Tool to aid one to learn Maori and enable one to program in Maori.

Learn Maori at https://maoridictionary.co.nz/

## Installation

Execute interactive terminal in docker

`docker run --rm -it registry.gitlab.com/samuel-garratt/te_reo_maori`

### As a ruby gem

Add this line to your application's Gemfile:

```ruby
gem 'te_reo_maori'
```

And then execute:

    $ bundle install

Or install it yourself as:

    $ gem install te_reo_maori

## Usage

Count in maori

```ruby
tahi + rua
# Will give a result of 3
3.whakahua
# Will return 'toru'
whakahua(tahi + tahi)
# Will return 'rua' 
```

Execute in interactive terminal. After installing type `te_reo_maori` on command prompt to open it.

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitLab at https://gitlab.com/samuel-garratt/te_reo_maori. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [code of conduct](https://gitlab.com/samuel-garratt/te_reo_maori/blob/master/CODE_OF_CONDUCT.md).


## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the TeReoMaori project's codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://gitlab.com/samuel-garratt/te_reo_maori/blob/master/CODE_OF_CONDUCT.md).
