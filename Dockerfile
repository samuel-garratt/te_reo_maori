FROM ruby:3-alpine
LABEL AUTHOR="Samuel Garratt"

RUN mkdir /app
WORKDIR /app
COPY . /app
RUN gem install bundler
RUN bundle config set --local without 'development'
RUN bundle install

ENTRYPOINT ["ruby", "exe/te_reo_maori"]
